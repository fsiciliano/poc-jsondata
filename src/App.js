import React from "react";
import Menu from './Components/Menu.jsx';
import PocOne from './Pages/poc-one';
import PocTwo from './Pages/poc-two';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

export default function App() {
  return (
    <Router>
      <div>
        <Menu />
        <Switch>
          <Route path="/editor1">
            <PocOne />
          </Route>
          <Route path="/editor2">
            <PocTwo />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
