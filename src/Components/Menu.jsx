import { Link } from "react-router-dom";

function Menu () {
    return (
        <ul>
            <li>
                <Link to="/editor1">Editor 1</Link>
            </li>
            <li>
                <Link to="/editor2">Editor 2</Link>
            </li>
        </ul>
    )
}

export default Menu