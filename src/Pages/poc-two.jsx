import React from "react";
import JSONInput from 'react-json-editor-ajrm';
import locale from 'react-json-editor-ajrm/locale/pt';
import initialValue from "../data/initialValue";


function PocTwo() {
    const handleChange = (e) => {
        console.log(e);
    }

    return (
        <JSONInput
            locale={locale}
            placeholder={initialValue}
            width="100%"
            onChange={(e) => handleChange(e)}
        />
    );
}

export default PocTwo