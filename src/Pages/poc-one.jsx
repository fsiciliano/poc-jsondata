import React from "react";
import { JsonEditor as Editor } from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';
import initialValue from "../data/initialValue";


function PocOne () {
    const handleChange = (e) => {
        console.log(e);
    }

    return (
        <Editor
            value={initialValue}
            onChange={(e) => handleChange(e)}
        />
    );
}

export default PocOne